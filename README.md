# goo24gl65

This project provides Sage tools for studying the groups of order 24 as subgroups of GL(6,5).

Copy the content of the text file to the Sagemath cell:

https://sagecell.sagemath.org/

or a Sagemath worksheet in your Cocalc account:

https://cocalc.com/

Then just evaluate the content.


